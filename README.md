## Interactive visual analytics using Sencha Charts

by _Vitaly Kravchenko_ at [SenchaCon 2015](http://senchacon.com/)

To run the examples please use the following URL syntax:

```
index.html?id=infographic_1
index.html?id=infographic_2
...
index.html?id=infographic_10
```